# 🤖 Quarantine Bot

## Table of Contents

 1. [Overview](#Overview)
    * 	[Assumptions](#Assumptions)
	  *   [Conversational Design](#Conversational-Design)
 2. [Getting Started](#Getting-Started)
    * 	[Prerequisites](#Prerequisites)
    * 	[Installation](#Installation)
 3. [Usage](#Usage)
    *   [Collaboration](#Collaboration)
    *   [Fulfillment and Integration](#Fulfillment-and-Integration)
    *   [Testing](#Testing)
 5. [Contributing](#Contributing)
 6. [License](#License)
 7. [Acknowledgements](#Acknowledgements)
 8. [Contact](#Contact)

## Overview
With the Covid-19 pandemic still rampant, many of the global population are currently subject to self-isolation. In most cases, these are COVID-19 patients with mild symptoms sent home by physicians to recover and do self-quarantine. And some are people who traveled to other places and need to undergo mandatory quarantine.

People who undergo self-quarantine are still needed to be continuously monitored and assessed consistently. The physicians must have a way to do close monitoring and routine consultation with patients in a fast and safe way. Apart from physical health, people under quarantine also need to have companion support to mitigate the heightened levels of loneliness, stress, and anxiety during isolation. This is when a virtual bot can be uniquely useful. 

**Quarantine Bot** is a chatbot developed using Dialogflow CX that aims to assist individuals and patients under home quarantine because of the COVID-19 virus. This hopes to be a help to ease the pressures the COVID-19 pandemic has placed on healthcare workers by allowing them to focus their care on more serious cases.

This is an entry for the **[2021 Dialogflow CX Global competition](https://events.withgoogle.com/dialogflow-cx-competition-global/?utm_source=medium_post&utm_medium=site&utm_campaign=cx_competition)**.

### Assumptions
This project only covers the conversational design of the bot and does not include the integrations and deployment. 

### Conversational Design
The conversation design used in this project is a combination of linear and non-linear conversation approaches. In the conversation with the user, the bot suggests related topics or questions that the user may ask, but the user can still respond or throw in questions that are not in suggestions.

## Getting Started

### Prerequisites
To be able to use this project, you will need to:
* Create a Google Cloud project
* Enable billing
* Enable Dialogflow API
* Create a Dialogflow CX agent

For more information on setting up your agent, please refer to the [Dialogflow CX Quickstarts](#https://cloud.google.com/dialogflow/cx/docs/quick).

### Installation
1. **Clone or download this repository**. If you downloaded a `.zip` file, unzip or extract it first.
2. Go to the [Dialogflow CX Console homepage](https://dialogflow.cloud.google.com/cx/projects) and **select your GCP project**.
3. In the list of agents within the project, **look for the CX agent that you have created and click the 3 dots on the right** (next to the Region column).
4. Click **Restore.**
5. Click the **Upload** radio button and select the blob file from your computer. (You may also upload from Google Cloud Storage.)
6. Click **Restore.**

## Usage
### Collaboration
This project has three main flows which each flow can be managed by a member of the team. 
* **Main Flow**. This contains the welcome spiels and the list of tasks that the bot can do for the user. 
* **Symptoms Monitoring.** This contains checking the measurement of vital health parameters, asks how the patient is feeling physically, and checks for signs or symptoms the patient may develop while under quarantine.
* **Companion and Support.** This contains responses and suggestions of the bot to the user's sentiments. This way patients can offload stress from their day when they may not want or be able to share such thoughts with family and friends.
* **Information Dissemination.** This contais the FAQs about the COVID-19 virus. And also suggestions on how to prevent the virus from spreading in a household, the treatments based from the experts, and safe care while at home.

### Fulfillment and Integration
#### Dialogflow Messenger
We utilize the [Dialogflow Messenger](https://cloud.google.com/dialogflow/cx/docs/concept/integration/dialogflow-messenger#fulfillment) text fulfillments to make the user experience better by including rich content in the bot's response.

#### Physician / Doctor Dashboard Integration
For future improvements, there must be a way to send the data entered by the user to the physician or doctor through an integration, preferably to a dashboard interface. And in response, the physician can reply with his or her assessment to the user.

### Testing
#### Run index.html to local server
* Using [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) for Visual Studio Code or
* Using [Node.js](https://nodejs.org/en/) http-server or `http-server . -p 8000`
* Using [Python](https://www.python.org/) server `python -m http.server`

#### Or import the blob file
Import the blob file to a Dialogflow CX project. And [setup and test](https://cloud.google.com/dialogflow/cx/docs/concept/integration/dialogflow-messenger#fulfillment) it using Dialogflow Messenger integration in the console.

#### Then start using the Quarantine Bot! ✨
Start the conversation with the bot by simply saying "Hi!" or "Hello".

## Contributing
Contributions are welcome. For more information, please refer to the [CONTRIBUTING.md](CONTRIBUTING.md).

## License
This project is licensed under the terms of the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).

## Acknowledgements
We would like to thank Google and Dialogflow CX team for acknowledging our idea and for giving us the opportunity to showcase it to the community.

## Contact
For queries and concerns, you may send an email to cx-competition-support@senti.com.ph
